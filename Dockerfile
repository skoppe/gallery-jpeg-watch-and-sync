FROM l3iggs/archlinux:latest

MAINTAINER skoppe <bas@21brains.com>

RUN pacman -S --noconfirm freeimage

# ENV PHOTO_STORAGE_SSL TRUE
ENV STORAGE_SERVICE_HOST 127.0.0.1
ENV STORAGE_SERVICE_PORT 3577

WORKDIR /app
ADD photo-watcher /app/photo-watcher

VOLUME ["/photos"]

CMD ./photo-watcher --input=/photos