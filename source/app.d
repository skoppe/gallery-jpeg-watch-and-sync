import vibe.d;
import vibe.core.driver;
import derelict.freeimage.freeimage;
import std.stdio : writeln, writefln;
import observe;

// This program listens for /jpe?g$/i files in the input folder and
// resizes each file (that isn't in the output folder) to max 1024px
// stores it in the output folder and then sends it to the wep-app
auto watchFolder(string folder)
{
	auto subject = new FiberSubject!DirectoryChange();
	DirectoryChange[1] change_buf;

	runTask({
		auto watcher =  watchDirectory(folder,true);
		DirectoryChange[] changes = change_buf[];
		while (true)
		{
			if (watcher.readChanges(changes)){
				import std.regex;
				auto ctr = ctRegex!(`(jpe?g|JPE?G|jpe|JPE)$`);
				if (matchFirst(changes[0].path.toNativeString,ctr))
					subject.onNext(changes[0]);
			}
		}
	});
	return subject;
}
Path getOutputPath(Path file, Settings settings)
{
	return Path(settings.output~"/"~file[1..$].toString());
}
struct Operation
{
	Path input;
	Path output;
	ResizeResult resized;
}
struct ResizeResult
{
	int width;
	int height;
	bool error = false;
	string digest;
}
auto makeOperation(Path file, Settings settings)
{
	return Operation(file,getOutputPath(file,settings));
}
bool notProcessed(Operation file)
{
	import std.file : exists;
	return !exists(file.output.toString());
}
auto writeThumbnail(I)(I image, int maxSize, string filename)
{
	auto output = filename.dup ~ "\0";
	ResizeResult rr;
	rr.width = FreeImage_GetWidth(image);
	rr.height = FreeImage_GetHeight(image);
	if (rr.width <= maxSize && rr.height <= maxSize)
	{
		rr.error = cast(bool)FreeImage_Save(FIF_JPEG,image,cast(char*)output,JPEG_QUALITYSUPERB);
		return rr;
	}

	auto thumb = FreeImage_MakeThumbnail(image,maxSize);
	scope (exit) FreeImage_Unload(thumb);
	rr.width = FreeImage_GetWidth(thumb);
	rr.height = FreeImage_GetHeight(thumb);
	rr.error = cast(bool)FreeImage_Save(FIF_JPEG,thumb,cast(char*)output,JPEG_QUALITYSUPERB);

	import std.digest.sha;
	SHA256 digest;
	import std.stdio;
	auto f = File(filename,"r");
	foreach(data; f.byChunk(4096))
		digest.put(data);
	import std.conv;
	rr.digest = to!string(toHexString(digest.finish())[0..64]);
	return rr;
}
auto processInputFile(Operation file)
{
	import std.file : mkdirRecurse;
	mkdirRecurse(file.output[0..$-1].toString());
	auto input = file.input.toString()~"\0";
	auto image = FreeImage_Load(FIF_JPEG,cast(char*)input);
	file.resized = writeThumbnail(image,1024,file.output.toString());
	FreeImage_Unload(image);
	return file;
}
auto waitFor2Seconds(I)(I input)
{
	import vibe.core.core : sleep;
	import core.time : msecs;
	sleep(2000.msecs);
	return input;
}
class DownloadError : Exception
{
	this(string url, int statusCode)
	{
		super("Download Error `"~url~"`, status: "~to!string(statusCode));
	}
}
auto sendFileOverWire(string url, Path file)
{
	struct Result
	{
		Path file;
	}
	import std.exception;
	return new Observer!(Result)
		((self){
			try
			{
				requestHTTP(url,
					(scope req)
					{
						import std.stdio;
						writefln("Sending file %s",file.toString());
						req.contentType = "image/jpeg";
						req.method = HTTPMethod.POST;
						foreach (ubyte[] b; File(file.toString()).byChunk(1024))
							req.bodyWriter.write(b);
					},
					(scope res)
					{
						writefln("Response %s",res);
						scope(exit) { res.dropBody(); }
						if (res.statusCode < 200 || res.statusCode > 299)
						{
							self.onError(new DownloadError(url,res.statusCode));//DownloadError.FAIL,res.statusCode));
							return;
						}
						self.onNext(Result(file));
						self.onCompleted();
					}
				);
			} catch (Exception e)
			{
				self.onError(e);
			}
		});
}
struct Meta
{
	string digest;
	string filename;
	int width;
	int height;
}
class MetaStore
{
	import std.stdio;
	import std.algorithm;
	import std.conv;
	private Meta[] data;
	private string filename;
	this(string filename)
	{
		import std.file;
		this.filename = filename;
		if (exists(filename))
			data = File(filename).byLine.joiner.text.parseJsonString.deserializeJson!(Meta[]);
	}
	void add(Meta m)
	{
		data ~= m;
		store();
	}
	void store()
	{
		auto f = File(filename,"w");
		scope (exit) f.close();
		import vibe.data.json;
		serializeToJson(f.lockingTextWriter(),data);
	}
}
struct Settings
{
	string input;
	string output;
	string host;
	ushort port;
	bool ssl;
	bool validate()
	{
		import std.file;
		Path path = Path(input);
		input = path.toNativeString();
		Path path2 = Path(output);
		output = path2.toNativeString();
		if (!(input.exists && input.isDir))
		{
			writefln("Invalid input folder `%s`",input);
			return false;
		}
		output.mkdirRecurse();
		return true;
	}
}
auto readSettings()
{
	import std.process;
	string input = environment.get("WATCHER_INPUT_FOLDER", "./input");
	string output = environment.get("WATCHER_OUTPUT_FOLDER", "./output");
	string host = environment.get("PHOTO_STORAGE_HOST", "127.0.0.1");
	ushort port = environment.get("PHOTO_STORAGE_PORT", "8080").to!ushort;
	bool ssl = environment.get("PHOTO_STORAGE_SSL", "FALSE") != "FALSE";
	auto set = Settings(input,output,host,port,ssl);

	readOption(
		"input",
		&set.input,
		"input folder"
	);
	readOption(
		"output",
		&set.output,
		"output folder"
	);
	readOption(
		"ssl",
		&set.ssl,
		"use SSL connection"
	);
	readOption(
		"host",
		&set.host,
		"upstream host"
	);
	readOption(
		"port",
		&set.port,
		"upstream host's port"
	);
	return set;
}
void main()
{
	auto settings = readSettings();
	if (!finalizeCommandLineOptions() || !settings.validate())
		return;

	auto upstream = (settings.ssl ? "https://" : "http://") ~ settings.host ~ ":" ~ settings.port.to!string;
	DerelictFI.load();
	FreeImage_Initialise();
	auto fileWatcher = watchFolder(settings.input).filter!(c=>c.type == DirectoryChangeType.modified).map(c=>c.path);
	auto sendQueue = new FiberSubject!Path();
	auto metaQueue = new FiberSubject!Meta();
	auto metaStore = new MetaStore(settings.input~"/meta.json");

	runTask({
		fileWatcher
			.map!(waitFor2Seconds)
			.map!((file){return makeOperation(file,settings);})
			.filter!(notProcessed)
			.map!(processInputFile)
			.subscribe((o)
				{
					metaQueue.onNext(Meta(o.resized.digest,o.input.toString(),o.resized.width,o.resized.height));
					sendQueue.onNext(o.output);
				});

		metaQueue
			.subscribe((m){ metaStore.add(m); });

		sendQueue
			.fork(10)
			.concatMap!((path){ return sendFileOverWire(upstream~"/uploads",path).retry!((_){writeln("Failed to send file. Retrying...");});})
			.subscribe(
				(o)
				{
					writefln("Synced file %s",o.file);
				},
				(e)
				{
					writefln("Failed to send file",e);
				});
	});

	runTask({
		import std.file;
		auto dFiles = dirEntries(settings.input,"*.{jpeg,JPEG,jpg,JPG,jpe,JPE}",SpanMode.depth);
		foreach(d; dFiles)
		{
			writefln("Found find %s",d.name);
			fileWatcher.subject.onNext(Path(d.name));
			import vibe.core.core : sleep;
			import core.time : msecs;
			sleep(2000.msecs);
		}
	});
	runEventLoop();
	FreeImage_DeInitialise();
}