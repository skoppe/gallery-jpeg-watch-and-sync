module observe;

import vibe.d;
import vibe.core.core;
import vibe.core.concurrency;
//import vibe.core.drivers.native;	// had to add this to get rdmd working
//import vibe.core.drivers.win32;		// had to add this to get rdmd working
import std.traits;
import std.functional;
import std.algorithm;
import std.range;
import std.stdio;
import core.atomic;

template ElementType(R)
{
	static if (isInputRange!R)
	{
		alias std.range.ElementType!R ElementType;
	} else
		alias ParameterTypeTuple!(R.NextFunc)[0] ElementType;
}
alias ElementType PayloadType;
template isObservable(O)
{
    enum bool isObservable = is(typeof(
    (inout int = 0)
    {
        O o = void;       // can define a observable object
        alias ElementType!O E;
        o.subscribe((scope E e){},(Exception e){},(){});	// has subscribe function
    }));
}
template isSubject(S)
{
	enum bool isSubject = is(typeof(
	(inout int = 0)
	{
		S s = void;
		alias ElementType!S E;
		E e;
		s.onNext(e);
		s.onError(new Exception("bla"));
		s.onCompleted();
	}));
}
alias void delegate (Exception) ErrorFunc;
alias void delegate () CompletedFunc;

struct Subscription(Observable)
{
	alias void delegate (ElementType!Observable ) NextFunc;
	private{ulong idx;
	Observable o;
	NextFunc onNext;
	ErrorFunc onError;
	CompletedFunc onCompleted;
	bool active = true;
	};
	void dispose()
	{
		if (active && o)
		{
			active = false;
			o.dispose(this);
			o = null;
		}
	}
}
class ObservableRange(R)
{
	alias void delegate (ElementType!R) NextFunc;
	alias Subscription!(ObservableRange!R) Sub;
	private
	{
		Sub[] subscriptions;
		R range;
		ulong uid;
	}
	this(R r)
	{
		this.range = r;
	}
	Sub subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
	{
		auto sub = Sub(uid++,this,onNext,onError,onCompleted);
		subscriptions ~= sub;
		if (onNext is null)	// why is this again?
			return sub;
		runTask(
		{
			foreach(e; this.range)
			{
				bool inactiveSubs = false;
				auto subs = subscriptions.dup();
				foreach(s; subs)
				{
					if (!s.active)
					{
						inactiveSubs = true;
						continue;
					}
					if (s.onNext)
						s.onNext(e);
				}
			}
			foreach(s; subscriptions)
				if (s.onCompleted && s.active)
					s.onCompleted();
			subscriptions.destroy();
		});
		return sub;
	}
	Sub subscribe(S)(S obs)
		if (isSubject!S)
	{
		return this.subscribe(&obs.onNext,&obs.onError,&obs.onCompleted);
	}
	void dispose(Sub s)
	{
		if (this != s.o)
			return;
		// simply mark the subscription in the list as inactive
		this.subscriptions = subscriptions.remove!(s2=>s2.idx == s.idx).array();
	}
}
ObservableRange!R observe(R)(R r)
	if (isInputRange!R || isIterable!R)
{
	return new ObservableRange!R(r);
}
alias observe observeRange;
unittest
{
	assert(isObservable!(typeof(iota(1, 10).observe)));
}
class Subject(E)
{
	alias E ElementType;
	alias void delegate (ElementType) NextFunc;
	alias Subscription!(Subject!E) Sub;
	private
	{
		Sub[] subscriptions;
		bool completed = false;
		ulong uid;
	}
	void onNext(ElementType e)
	{
		foreach(s; subscriptions)
			if (s.onNext && s.active)
				s.onNext(e);
	}
	void onError(Exception e)
	{
		Sub[] ss = subscriptions.dup();
		subscriptions.destroy();
		foreach(s; ss)
			if (s.onError && s.active)
				s.onError(e);
	}
	void onCompleted()
	{
		if (completed)
			return;
		completed = true;
		foreach(s; subscriptions)
			if (s.onCompleted && s.active)
				s.onCompleted();
	}
	Sub subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
	{
		auto sub = Sub(uid++,this,onNext,onError,onCompleted);
		subscriptions ~= sub;
		return sub;
	}
	Sub subscribe(S)(S obs)
		if (isSubject!S)
	{
		return this.subscribe(&obs.onNext,&obs.onError,&obs.onCompleted);
	}
	void dispose(Sub s)
	{
		if (s.o != this || completed)
			return;
		subscriptions = subscriptions.remove!(s2 => s2.idx == s.idx).array();
		s.onCompleted();
	}
}
unittest
{
	assert(is(ElementType!(Subject!int) : int));
	assert(isObservable!(Subject!int));
	assert(isSubject!(Subject!int));
}
unittest
{
	auto s = new Subject!int;
	s.subscribe((i) => assert(i == 1));
	s.onNext(1);
}
unittest
{
	auto s = new Subject!int();
	int cnt = 0;
	auto sub = s.subscribe((m){assert(m == 1);},null,(){cnt++; assert(cnt == 1);});
	s.onNext(1);
	sub.dispose();
	s.onNext(2);
	s.onCompleted();
}
unittest
{
	auto s = new Subject!int();
	int cnt = 0;
	auto sub = s.subscribe((m){assert(m == 1);},null,(){cnt++; assert(cnt == 1);});
	s.onNext(1);
	s.onCompleted();
	sub.dispose();
}
// maybe instead a subject should consist of a Observer and an Observable, instead of the other way around.
// where the Observer whould have the onNext,onError and onCompleted and the Observable only the subscribe
class Observer(E) : Subject!E
{
	alias void delegate(Observer!(E)) Subscriber;
	private
	{
		bool subscribed = false;
		Subscriber subscriber;
	}
	this(Subscriber subscriber)
	{
		this.subscriber = subscriber;
	}
	override void onError(Exception e)
	{
		this.subscribed = false;
		super.onError(e);
	}
	override Sub subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
	{
		auto sub = super.subscribe(onNext,onError,onCompleted);
		if (!subscribed)
		{
			subscribed = true;
			subscriber(this);
		}
		return sub;
	}
}
// here we test some basic UFCS and unaryFun behaviour that we rely on
template foo(alias fun)
{
	auto foo(T)(T n)
	{
		alias func = unaryFun!fun;
		return func(n);
	}
}
unittest
{
	assert(foo!(a => a*a)(4) == 16);
	assert(4.foo!(a => a*a) == 16);
	assert(foo!"a*a"(4) == 16);
	assert(4.foo!"a*a" == 16);
	auto square = (int n) { return n*n; };
	assert(foo!square(4) == 16);
	assert(4.foo!square == 16);
}
template map(alias fun)
{
	auto map(Obs)(Obs observable)
		if (isObservable!Obs)
	{
		alias func = unaryFun!fun;
		alias AppliedReturnType = typeof(func(ElementType!(Obs).init));

		return new Observer!(AppliedReturnType)
			((self){
				observable.subscribe(
					(e){ self.onNext(func(e)); },
					&self.onError,
					&self.onCompleted
				);
			});
	}
}
//alias map oMap;
unittest
{
	auto s = new Subject!int;
	auto p = s.map!(s => "abs");
	p.subscribe((s){ assert(s == "abs");});
	s.onNext(1);
}
unittest
{
	bool completed = false;
	auto s = new Subject!int;
	auto q = s.map!(s => s*s).map!(s => s*2);
	q.subscribe((s) { assert(s == 18);},null,(){completed = true;});
	s.onNext(3);
	s.onCompleted();
	assert(completed == true);
}
unittest
{
	bool completed = false;
	auto s = new Subject!int;
	auto sub = s.subscribe((v) { assert(v == 18);},null,(){completed = true;});
	s.onNext(18);
	sub.dispose();
	assert(completed == true);
}
class FiberSubject(E) : Subject!E
{
	alias void delegate (E) NextFunc;
	private
	{
		Task t;
		bool started = false;
	}
	void join()
	{
		forceTask();
		t.join();
	}
	private void forceTask()
	{
		if (started)
			return;
		t = runTask(
		{
			bool running = true;
			while (running) {
				receive(
					(E e)
					{
						foreach(s; subscriptions)
							if (s.onNext && s.active)
								s.onNext(e);
					},
					(Exception e)
					{
						foreach(s; subscriptions)
							if (s.onError && s.active)
								s.onError(e);
						running = false;
					},
					(bool)
					{
						foreach(s; subscriptions)
							if (s.onCompleted && s.active)
								s.onCompleted();
						running = false;
					}
				);
			}
		});
		started = true;
	}
	override void onNext(E e)
	{
		forceTask();
		t.send(e);
	}
	override void onError(Exception e)
	{
		forceTask();
		t.send(e);
	}
	override void onCompleted()
	{
		forceTask();
		t.send(true);
	}
}
unittest
{
	auto s = new FiberSubject!int;
	s.subscribe((s) { assert(s == 14);},null,(){exitEventLoop();});
	s.onNext(14);
	s.onCompleted();
	runEventLoop();
}
unittest
{
	auto s = new FiberSubject!int;
	auto q = s.map!(s=>s*s);
	q.subscribe((s) { assert(s == 28*28);},null,(){exitEventLoop();});
	s.onNext(28);
	s.onCompleted();
	runEventLoop();
}
Observer!(Tuple!(ElementType!ObsA,ElementType!ObsB)) zip(ObsA, ObsB)(ObsA a, ObsB b)
	if (isObservable!ObsA && isObservable!ObsB)
{
	auto result = new Observer!(Tuple!(ElementType!ObsA,ElementType!ObsB))
		((self){
			import vibe.core.sync;
			auto event = createManualEvent();
			int emitA = event.emitCount();
			int emitB = emitA;
			ElementType!ObsA objA;
			bool complete = false;
			a.subscribe((ae) 
				{
					objA = ae;
					event.emit();
					emitA = event.wait(emitA + 1);
				},
				(e) { self.onError(e); },	// TODO: unsubscribe from underlying observable
				() { complete = true; event.emit(); self.onCompleted(); }
			);
			b.subscribe((be) 
				{
					emitB = event.wait(emitB) + 1;
					if (!complete)
						self.onNext(Tuple!(ElementType!ObsA,ElementType!ObsB)(objA,be));
					event.emit();
				},
				(e) { self.onError(e); }, // TODO: unsubscribe from underlying observable
				() { event.emit(); self.onCompleted(); }
			);
		});

	return result;
}
unittest
{
	auto s1 = new FiberSubject!int;
	auto s2 = new FiberSubject!string;
	auto p = s1.zip(s2);
	p.subscribe((t) { assert(t[0] == 1); assert(t[1] == "abc"); },null,(){exitEventLoop();});
	s1.onNext(1);
	s2.onNext("abc");
	s2.onNext("abc");
	s1.onNext(1);
	s1.onCompleted();
	s2.onCompleted();
	runEventLoop();
}
unittest
{
	auto range = iota(0, 10);
	auto s1 = new FiberSubject!int;
	auto s = range.observe.zip(s1);
	s.subscribe((t) { assert(t == tuple(0,5)); },null,(){exitEventLoop();});
	s1.onNext(5);
	s1.onCompleted();
	runEventLoop();
}

template filter(alias fun)
{
	alias func = unaryFun!fun;
	auto filter(Obs)(Obs observable)
		if (is(typeof(func(ElementType!(Obs).init)) : bool) && isObservable!Obs)
	{
		return new Observer!(ElementType!Obs)
			((self){
				observable.subscribe
				(
					(i) { if (func(i)) self.onNext(i); },
					&self.onError,
					&self.onCompleted
				);
			});
	}
}
unittest
{
	// somehow this has tasks still running at exit
	auto range = iota(1, 10);
	range
		.observe
		.map!((i){return i*i;})
		.filter!(i=>i==81)
		.subscribe((i) { assert(i == 81); },null,(){exitEventLoop();});
	runEventLoop();
}

unittest
{
	auto range = iota(1, 3);
	auto s = range.observe.filter!(i=>i % 2 == 0);
	s.subscribe((t) { assert(t == 2); },null,(){exitEventLoop();});
	runEventLoop();
}

unittest
{
	auto event = createManualEvent();
	assert(event.wait(40) == 0);
	event.emit();
	assert(event.wait(40) == 1);
}
unittest
{
	auto event = createManualEvent();
	event.emit();
	assert(event.wait(0) == 1);
}
class Fork(E) : Observer!E
{
	private
	{
		shared int legs;
		int size;
		ReturnType!createManualEvent event;
	}
	this(int legs, Subscriber s)
	{
		super(s);
		this.legs = legs;
		size = legs;
		event = createManualEvent();
	}
	override void onCompleted()
	{
		int cnt = event.emitCount();
		while (cnt+size > legs)
			cnt = event.wait(cnt);
		super.onCompleted();
	}
	override void onNext(E e)
	{
		event.wait(legs);
		legs.atomicOp!"-="(1);
		runTask(
			{
				foreach(s; subscriptions)
					if (s.onNext && s.active)
						s.onNext(e);
				legs.atomicOp!"+="(2);
				event.emit();
			});
	}
}

Fork!(ElementType!Obs) fork(Obs)(Obs o, int legs)
	if (isObservable!Obs)
{
	import std.exception;
	enforce(legs > 0, "can't fork without legs");
	return new Fork!(ElementType!Obs)(legs,
		(self){
			o.subscribe(
				&self.onNext,
				&self.onError,
				&self.onCompleted
			);
		});
}


unittest
{
	int cnt = 0;
	iota(0,10).observe.fork(10).subscribe((e){ cnt += e; },null,() {exitEventLoop(); assert(cnt == 45); });
	runEventLoop();
}

Observer!(ElementType!ObsA) chain(ObsA, ObsB)(ObsA a, ObsB b)
	if (isObservable!ObsA && isObservable!ObsA && is (ElementType!ObsA : ElementType!ObsB))
{
	return new Observer!(ElementType!ObsA)
		((self)
		{
			auto event = createManualEvent();
			a.subscribe(
				&self.onNext,
				&self.onError,	// TODO: instead we need to unsubscribe from b, then propagate error
				() { event.emit(); if (event.emitCount() == 2) self.onCompleted(); }
			);
			b.subscribe(
				&self.onNext,
				&self.onError,	// TODO: instead we need to unsubscribe from a, then propagate error
				() { event.emit(); if (event.emitCount() == 2) self.onCompleted(); }
			);
		});
}

unittest
{
	auto s = new FiberSubject!int;
	int cnt = 0;
	iota(0,10).observe.chain(s).subscribe((e){ cnt += e; },null,() {exitEventLoop(); assert(cnt == 76); });
	s.onNext(15);
	s.onNext(16);
	s.onCompleted();
	runEventLoop();
}
class ConcatMap(T, alias func)
{
	alias AppliedReturnType = typeof(func(T.init));
	alias ElementType!(AppliedReturnType) E;
	alias void delegate (E) NextFunc;
	alias void delegate(ConcatMap!(T, func)) SubscribeFunc;
	alias Subscription!(ConcatMap!(T, func)) Sub;
	private
	{
		bool subscribed = false;
		SubscribeFunc subscriber;
		Sub[] subscriptions;
		shared int cnt;
		ulong idx;
	}
	this(SubscribeFunc sFunc)
	{
		this.subscriber = sFunc;
		this.cnt = 1;
	}
	private void subCompleted()
	{
		if (cnt.atomicOp!"-="(1) != 0)
			return;
		foreach (s; subscriptions)
			if (s.onCompleted)
				s.onCompleted();
		subscriptions.destroy();
	}
	private void subOnNext(E e)
	{
		foreach (s; subscriptions)
			if (s.onNext)
				s.onNext(e);
	}
	private void subOnError(Exception e)
	{
		foreach (s; subscriptions)
			if (s.onError)
				s.onError(e);
	}
	void onNext(T e)
	{
		cnt.atomicOp!"+="(1);
		auto obs = func(e);
		obs.subscribe(&this.subOnNext,&this.subOnError,&this.subCompleted);
	}
	void onError(Exception e)
	{
		foreach (s; subscriptions)
			if (s.onError)
				s.onError(e);
	}
	void onCompleted()
	{
		subCompleted();
	}
	Sub subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
	{
		auto sub = Sub(idx++,this,onNext,onError,onCompleted);
		subscriptions ~= sub;
		if (!subscribed)
		{
			subscribed = true;
			subscriber(this);
		}
		return sub;
	}
	void dispose(Sub s)
	{
		// todo: make work
	}
}
template concatMap(alias fun)
{
	alias func = unaryFun!fun;

	auto concatMap(Obs)(Obs o)
		if (isObservable!Obs && isObservable!(typeof(func(ElementType!(Obs).init))))
	{
		return new ConcatMap!(ElementType!Obs,func)(
			(self)
			{
				o.subscribe(
					&self.onNext,
					&self.onError,
					&self.onCompleted);
			});
	}
	auto concatMap(Obs)(Obs o)
		if (isObservable!Obs && isInputRange!(typeof(func(ElementType!(Obs).init))))
	{
		return o.concatMap!(a => func(a).observe())();
	}
}
unittest
{
	auto s = iota(0,5).observe;
	size_t c;
	s.concatMap!(s => iota(0,5).observe() ).subscribe((p){c += p;},null,(){ exitEventLoop(); assert(c == 50); });
	runEventLoop();
}
unittest
{
	auto s = iota(0,5).observe;
	size_t c;
	s.concatMap!(s => iota(0,5) ).subscribe((p){c += p;},null,(){ exitEventLoop(); assert(c == 50); });
	runEventLoop();
}
//class ObservableTimer
//{
//	alias void delegate (int) NextFunc;
//	private
//	{
//		uint delay, period;
//	}
//	this(uint d, uint p)
//	{
//		this.delay = d;
//		this.period = p;
//	}
//	void subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
//	{
//		if (onNext is null)
//			return;
//		runTask(
//		{
//			sleep(this.delay);
//			ulong cnt;
//			while (true)
//			{
//				onNext(cnt);
//				cnt++;
//				sleep(period);
//			}
//		});
//	}	
//}
//auto timer(int delay, int period)
//{
//	return new ObservableTimer(delay,period);
//}
template tap(alias fun)
{
	alias func = unaryFun!fun;

	auto tap(Obs)(Obs o)
		if (isObservable!Obs && is(typeof(func(ElementType!(Obs).init)) : void))
	{
		return new Observer!(ElementType!Obs)(
			(self)
			{
				o.subscribe(
					(e){ func(e); self.onNext(e); },
					&self.onError,
					&self.onCompleted);
			});
	}
	//auto tap(Obs)(Obs o)
	//	if (isObservable!Obs && isObservable!(typeof(func(ElementType!(Obs).init))))
	//{
	//	return new Observer!(ElementType!Obs)(
	//		(self)
	//		{
	//			// the o observable might trigger onCompleted before the observable returned by the func completes
	//			// in which case one or more onNext will be called AFTER onCompleted
	//			o.subscribe(
	//				(e){ func(e).subscribe(null,$self.onError,(){self.onNext(e);}); },
	//				&self.onError,
	//				&self.onCompleted);
	//		});
	//}
}
/*unittest
{
	auto s = iota(0,5).observe;
	size_t c;
	s.tap!((i) { c += i; }).subscribe((i){ c+= i; },null,(){ exitEventLoop(); assert(c == 20); });
	runEventLoop();
}*/

class Retry(Obs) : Observer!(ElementType!Obs)
{
	private
	{
		Obs obs;
		shared int triesLeft;
	}
	this(Obs o, int maxTries)
	{
		super((self){ o.subscribe(&self.onNext,&self.onError,&self.onCompleted); });
		obs = o;
		triesLeft = maxTries+1;
	}
	override void onError(Exception e)
	{
		if (triesLeft > -1) // because we did maxTries+1, this has to be -1 instead of the obvious 0
			triesLeft.atomicOp!"-="(cast(int)1);
		if (triesLeft == 0)
		{
			super.onError(e);
			return;
		}
		// onError causes subscriptions to be dropped
		// so we can simply subscribe again to trigger
		// a retry of the whole chain.
		obs.subscribe(&onNext,&onError,&onCompleted);
	}
}

auto retry(Obs)(Obs obs, int maxTries = -1)
	if (isObservable!Obs)
{
	return new Retry!(Obs)(obs, maxTries);
}

template retry(alias fun)
{
	auto retry(Obs)(Obs obs, int maxTries = -1)
		if (isObservable!Obs)
	{
		return obs.tap!(fun).retry(maxTries);
	}
}

unittest
{
	auto s = iota(0,5).observe;
	class Thrower : Subject!int
	{
		override void onNext(int e)
		{
			import std.conv : to;
			if (e < 4)
			{
				this.onError(new Exception(e.to!string));
				return;
			}
			super.onNext(e);
		}
	}
	auto t = new Thrower();
	t.retry().subscribe(
		(e){assert(e == 4);},
		(err){assert(false);},
		(){exitEventLoop();
	});
	s.subscribe(t);
	runEventLoop();
}

template filterError(alias fun)
{
	alias func = unaryFun!fun;

	auto filterError(Obs)(Obs o)
	{
		return new Observer!(ElementType!Obs)(
			(self)
			{
				o.subscribe(
					&self.onNext,
					(e){ if (!func(e)) self.onError(e); },
					&self.onCompleted);
			});
	}
}
unittest
{
	auto s = iota(0,5).observe;
	class Thrower : FiberSubject!int
	{
		override void onNext(int e)
		{
			this.onError(new Exception("dummy"));
		}
	}
	auto t = new Thrower();
	int foo = 0;
	// here the iota observable keeps firing, just nobody is listening, maybe we should stop that stream by disposing of each subscription in the chain
	t.filterError!((e){foo++;return false;}).subscribe(
		(e){assert(false);},
		(err){foo++;exitEventLoop();}
	);
	s.subscribe(t);
	runEventLoop();
	assert(foo==2);
}
class BufferSample(Src, Sampler) : Observer!(ElementType!(Src)[])
{
	alias Src.ElementType SourceElementType;
	private
	{
		SourceElementType[] buffer;
		Sampler.Sub sampleSubscription;
		Src.Sub sourceSubscription;
		bool sampleCompleted = false;
	}
	this(Src src, Sampler smpr)
	{
		// if sampler completes, this also completes.
		// if source completes, this also completes.
		// if the source completes, the buffer is discarded.
		super((self)
		{
			this.sourceSubscription = src.subscribe((e){this.buffer ~= e;},&self.onError,&self.onCompleted);
			this.sampleSubscription = smpr.subscribe(
				(smpl)
				{
					auto dup = this.buffer;
					this.buffer = [];
					super.onNext(dup);
				},
				(err)
				{
					sourceSubscription.dispose();
					this.onError(err);
				},
				()
				{
					sampleCompleted = true;
					sourceSubscription.dispose();
				});
		});
	}
	override void onError(Exception e)
	{
		this.buffer.destroy();
		super.onError(e);
	}
	override void onCompleted()
	{
		if (!sampleCompleted)
			sampleSubscription.dispose();
		this.buffer.destroy();
		super.onCompleted();
	}
}
auto bufferSample(Src, Sampler)(Src source, Sampler sampler)
	if (isObservable!Src && isObservable!Sampler)
{
	return new BufferSample!(Src,Sampler)(source,sampler);
}
unittest
{
	auto src = new FiberSubject!int;
	auto smp = new FiberSubject!int;
	int cnt = 0;
	src.bufferSample(smp).subscribe((e){assert(9==reduce!((a,b)=>a+b)(0,e));},null,(){cnt++;exitEventLoop();});
	src.onNext(4);
	src.onNext(5);
	smp.onNext(1);

	src.onNext(3);
	src.onNext(6);
	smp.onNext(1);

	src.onCompleted();
	smp.onCompleted();
	runEventLoop();
	assert(cnt == 1);
}
//class Catch(Obs) : Subject!(ElementType!Obs)
//{
//	private
//	{
//		bool subscribed = false;
//		Obs o;
//	}
//	this(Obs o)
//	{
//		this.o = o;
//	}
//	override void onError(Exception e)
//	{
//		onCompleted();
//	}
//	override void subscribe(NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
//	{
//		super.subscribe(onNext,onError,onCompleted);
//		if (!subscribed)
//		{
//			o.subscribe(&this.onNext,&this.onError,&this.onCompleted);
//			subscribed = true;
//		}
//	}
//}
//Catch!(Obs) catchException(Obs)(Obs o)
//	if (isObservable!Obs)
//{
//	return new Catch!(Obs)(o);
//}